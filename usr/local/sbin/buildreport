#!/usr/bin/env ruby
############
## MIT License
## Copyright (c) 2019-2020 Sebastien Delcroix (Seb) dev/at/delX/dot/io
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
############

require 'json'

class Report

    def initialize(files)
        
        @json_report = Array.new
        
        files.each do |file|
            @json_report << JSON.parse(File.read(file))
        end

    end


    def build()

        result = ""
        packages = ""
        host_info = Hash.new
        @json_report.each do |report|
            host_info = {}
            host_info[:hostname] = report['info']['server']['Static_hostname']
            #host_info[:type] = report['info']['server']['Chassis'].match(/vm/i) ? "VM" : "Host"
            host_info[:type] = report['info']['server'].has_key?("Virtualization") ? "VM" : "Host"
            host_info[:os] = report['info']['server']['Operating_System'].sub("GNU/Linux ","").sub(/\([^\)]+\)/,"")
            host_info[:reboot] = report['stage']['reboot']['status']
            host_info[:uptime] = report['info']['uptime'].split(" ")[0]
            host_info[:packages] = report['upgraded_packages'].length
            failed_list=[]
            report['stage'].each do |stage,data|

                if data['status'] == "failed"
                    failed_list << stage
                end

            end

            packages << "\n#{host_info[:hostname]}:\n"
            report['upgraded_packages'].each do |package,versions|
                packages << "#{package}: #{versions['oldver']} -> #{versions['newver']}\n"
            end

            result << "#{host_info[:hostname]}: #{host_info[:type]} / #{host_info[:os]} / uptime=#{host_info[:uptime]} / reboot=#{host_info[:reboot]} / packages=#{host_info[:packages]} / %s\n" % [ failed_list.length > 0 ? "FAILED=#{failed_list.join(',')}" : "OK"]

        end

        result << "\n"

        result << packages

        return result
    end

end


myreport = Report.new(ARGV)

puts myreport.build
