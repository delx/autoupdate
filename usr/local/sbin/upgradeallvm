#!/bin/bash
############
## MIT License
## Copyright (c) 2019-2020 Sebastien Delcroix (Seb) dev/at/delX/dot/io
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
############

PRG=$(basename $0)
VERSION="1.2.0"

admin_user="$1"
sudo_cmd="$2"

CURRENT=/var/lib/upgradeallvm/current
ARCHIVES=/var/lib/upgradeallvm/archives
REPORT_PATH=/var/lib/upgradesrv
UPGRADE_CMD=/usr/local/sbin/upgradesrv
REBOOT_CMD=/sbin/reboot
cur_date=$(date +"%Y-%m-%d")
TRACE=/var/lib/upgradeallvm/$cur_date.log

> $TRACE

if [ -e /etc/upgradenotifier ]
then
    . /etc/upgradenotifier
else
    NOTIFIER=rntfy
fi

archiveReport()
{
    mv "$CURRENT"/* "$ARCHIVES"/
}

sendReport()
{
    local 
    buildreport $CURRENT/* | $NOTIFIER "[$PRG $VERSION][$(hostname)] - $cur_date"

    ########### check if mail or ntfy ##########

}

logMe()
{
    local msg
    msg="$1"
    local cur_date
    cur_date=$(date --iso-8601=seconds)
    echo "[$cur_date]$msg" >> $TRACE
}

#Install ruby for reporting
if [ ! -e /usr/bin/ruby ]
then
    apt-get update -o quiet=2 2>&1 >> $TRACE
    apt-get -o quiet=2 -y install ruby 2>&1 >> $TRACE
fi

mkdir -p "$ARCHIVES"
mkdir -p "$CURRENT"

for vm in $(qm list | grep running | awk '{print $2}')
do
    logMe "[[$vm]autoupdate] starting update"
    rsync -a /root/install.sh $admin_user@$vm:/root/ 2>&1 >> $TRACE
    ssh $admin_user@$vm "$sudo_cmd/root/install.sh" 2>&1 >> $TRACE
    logMe "[$vm][autoupdate] update done"

    logMe "[$vm][upgrade] OS & App starting"
    ssh $admin_user@$vm "$sudo_cmd$UPGRADE_CMD noreboot" 2>&1 >> $TRACE
    logMe "[$vm][upgrade] OS & App done"

    logMe "[$vm][gathering report] starting"
    rsync -a $admin_user@$vm:"$REPORT_PATH"/* "$CURRENT/" 2>&1 >> $TRACE
    logMe "[$vm][gathering report] done"

    logMe "[$vm][archiving report] starting"
    ssh $admin_user@$vm "$sudo_cmd""mv $REPORT_PATH/* $ARCHIVES/" 2>&1 >> $TRACE
    logMe "[$vm][archiving report] done"

done

logMe "[host][upgrade] OS & App starting"
$UPGRADE_CMD noreboot
logMe "[host][upgrade] done"

last_report=$(ls -tr "$REPORT_PATH"/*| tail -1)

reboot_status=$(jq .stage.reboot.status "$last_report" | tr -d '""')

cp "$last_report" "$CURRENT"/

logMe "[host][report] sending"
sendReport
logMe "[host][report] done"


if [ "$reboot_status" = "required" ]
then
    # will reboot all VM and host
    logMe "[host][report] archiving"
    archiveReport
    logMe "[host][report] done"

    logMe "[_all_][reboot] starting"
    $REBOOT_CMD
else
    # will reboot only VMs need to be
    for report in "$CURRENT"/*
    do
        vm=$(basename $report | cut -d "_" -f 1)
        date=$(basename $report | cut -d "_" -f 2 | cut -d "." -f 1)
        reboot_status=$(jq .stage.reboot.status "$report" | tr -d '""')
        if [ "$reboot_status" = "required" ]
        then
            logMe "[$vm][rebooting] starting"
            ssh $admin_user@$vm "$sudo_cmd$REBOOT_CMD" 2>&1 >> $TRACE
            logMe "[$vm][rebooting] done"

        fi
    done
fi

logMe "[host][report] archiving"
archiveReport
logMe "[host][report] done"
